#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <sys/time.h>

#define True 1
#define False 0

void sieve_serial(int  n){
	int i,j; 
	int prime  = True; //prime flag used to get primes
	int * primes = malloc(n*sizeof(int)); //list of primes
	primes[0] = 2; //init
	primes[1] = 3;
	int num_primes = 2; //number of primes we have so far
	//printf("primes between 1 and %d are:\n", n);
	//printf("%d  ", primes[0]);
	//printf("%d  ", primes[1]);
	for (i=4; i<=n; i++){ //loop through numbers to test for primes
		prime = True; //assume i is prime until proven otherwise
		for (j=0; j<num_primes; j++){ //loop through primes so far
			//printf("%d\n", primes[j]);
			if (i%primes[j] == 0){ //if i not prime
				//printf("%d mod %d = 0\n", i, primes[j]);
				prime = False; //i isn't prime
				//printf("%d\n", primes[j]);
				j = num_primes; //break out of j loop without using a break statement
			}
		}
		if (prime == True){ //if i is prime
			//printf("%d  ",i); //print it
			primes[num_primes] = i; //add it to list of primes
			num_primes++; //increment number of primes so far
		}
	}
	free(primes);
}
void sieve_parallel(int  n){
	int i,j; 
	int prime  = True; //prime flag used to get primes
	int * primes = malloc(n*sizeof(int)); //list of primes
	primes[0] = 2; //init
	primes[1] = 3;
	int num_primes = 2; //number of primes we have so far
	//printf("primes between 1 and %d are:\n", n);
	//printf("%d  ", primes[0]);
	//printf("%d  ", primes[1]);
//	#pragma omp parallel for private(j) shared(prime) schedule(dynamic) //other variables shared too, not great for parallelization as its written 
	for (i=4; i<=n; i++){ //loop through numbers to test for primes
		prime = True; //assume i is prime until proven otherwise
		#pragma omp parallel for schedule(dynamic, i-3)
		for (j=0; j<num_primes; j++){ //loop through primes so far
			//printf("%d\n", primes[j]);
//			#pragma omp critical 
		//	{
			if (i%primes[j] == 0){ //if i not prime
				//printf("%d mod %d = 0\n", i, primes[j]);
				prime = False; //i isn't prime
				//printf("%d\n", primes[j]);
		//	}
				j = num_primes; //break out of j loop without using a break statement
			}
		}
		if (prime == True){ //if i is prime
		//	printf("%d  ",i); //print it
			primes[num_primes] = i; //add it to list of primes
			num_primes++; //increment number of primes so far
		}
	}
	free(primes);
}

int main(int argc, char * argv[]){
	int n, opt;
	if (argc != 3){
		printf("Please insert an argument as upper limit of primes, like so: ./a.out -n <max_prime_limit>\n");
	}
	while ((opt = getopt(argc, argv,"n:")) != -1) {
		switch (opt) {
			case 'n' : 
				n = atoi(optarg);
				break;
		}
	}
	//time serial
	struct timeval start, end;
	long long serial_time, parallel_time;
	gettimeofday(&start, NULL);
	sieve_serial(n);
	gettimeofday(&end, NULL);
	serial_time = (end.tv_sec - start.tv_sec)*1000000L + (end.tv_usec - start.tv_usec);
	printf("\n"); //using since no newline when printing primes
	printf("serial sieve took %lld microseconds\n", serial_time);

	//time parallel	
	gettimeofday(&start, NULL);
	sieve_parallel(n);
	gettimeofday(&end, NULL);
	parallel_time = (end.tv_sec - start.tv_sec)*1000000L + (end.tv_usec - start.tv_usec);
	printf("\n"); //using since no newline when printing primes
	printf("parallel sieve took %lld microseconds\n", parallel_time);

	//write results to file
	FILE * f = fopen("results.csv", "a");
	if (f == NULL) return -1;
	fprintf(f, "%d,%lld, %lld\n", n, serial_time, parallel_time);
	fclose(f);
	
	return 0;
}
