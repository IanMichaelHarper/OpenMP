#import matplotlib.pyplot as plt
import pylab as plt
import pandas as pd

df = pd.read_csv("results.csv")

#plot block time against block size for different numbers of processors
plt.plot(df["n"],df["serial time"],'o', linestyle='-', label="Serial Time") 
plt.plot(df["n"],df["parallel time"],'o', linestyle='-', label="Parallel Time") 
plt.xlabel("Number Limit n")
plt.ylabel("Time (us)")
plt.title("Time (in microseconds) vs Numbers to check up until")
plt.legend(loc="upper left", prop={'size':10})
plt.savefig("graph.ps")
