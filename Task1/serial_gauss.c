#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <sys/time.h>
#include <time.h>
//assuming square matrix for this assignment

void print_matvec(double ** A, double * b, int n){
	int i,j;	
	for (i=0; i<n; i++){
		for (j=0; j<n; j++)
			printf("%lf ", A[i][j]);
		printf("%lf \n", b[i]);
	}
	printf("\n");
}
//guassian elimination
void gauss(double ** A, double * b, int n){
	double div,mult;
	int i,j,k;
	for (i=0; i<n; i++){ //loop through rows
//		print_matvec(A,b,n);
//		if (i>300)
//			printf("i = %d\n", i);
		div = A[i][i];
		if (div == 0){
			printf("divisor is 0 in gauss(), exiting\n");
			exit(1);
		}
		for (j=i; j<n; j++){ //loop through columns and divide by ith diagonal entry to get pivot
			A[i][j] = A[i][j]/div; //divide by div to get 1 in pivot slot
		}
		b[i] = b[i]/div; //do same for b
//		print_matvec(A,b,n);
		for (k=0; k<n; k++){ //loop through rows again
			if (k!=i) {//skip pivot row
				mult = -A[k][i];
				for (j=i; j<n; j++){ //loop through columns to clear pivot cols 
					//if (A[k][j] == 0){
						//j=n; continue; //this line instead of break cuts the runtime in half for some reason
						//break; //skip row if already 0 in pivot col
					//}
					A[k][j] = A[k][j] + A[i][j]*mult; //multiply row by mult and add to get zeros in pivot col
				}
				b[k] = b[k] + b[i]*mult;
			}
		}
	}
}
double ** init_mat(int n){
	double ** A = malloc(n*sizeof(double *));
	double  * Acols = malloc(n*n*sizeof(double ));
	double row_sum; 
	double row_max;
	int i,j;
	srand(time(NULL));
	for (i=0; i<n; i++){
		A[i] = &Acols[i*n];
		row_sum = 0;
		row_max = 0;
		for (j=0; j<n; j++){
			A[i][j] = (double)(rand() % 10); //fill matrix with rand
			row_sum += row_sum + abs(A[i][j]); //sum absolute values of row values
			if (i==j) //set row_max first time
				row_max = abs(A[i][i]);
			if (abs(A[i][j]) > row_max) //if we find a value that is greater than row_max, reassign
				row_max = abs(A[i][j]); 
		}
		if (row_sum > abs(A[i][i]))  //if matrix is not diagonnaly dominant
				A[i][i] = row_max + 1; //ensure it is
	}
	return A;
}

double * init_vec(int n){
	double * b = malloc(n * sizeof(double));
	srand(time(NULL));
	for (int i=0; i<n; i++){
		b[i] = (double)(rand() % 10);
	}
	return b;
}
void print_mat(double ** A, int n){
	int i,j;	
	for (i=0; i<n; i++){
		for (j=0; j<n; j++)
			printf("%lf ", A[i][j]);
		printf("\n");
	}
}

void print_vec(double * b, int n){
	for (int i=0; i<n; i++){
		printf("%lf ", b[i]);
	}
	printf("\n");
}

	
int main(int argc, char * argv[]){
	int n, opt;
	if (argc != 3){
		printf("please enter the size of the matrix like so: ./a.out -n <size>\n");
	}
	while((opt = getopt(argc, argv, "n:")) != -1){
		switch (opt) {
			case 'n': 
				n = atoi(optarg);
				break;
		}
	}
	double ** A = init_mat(n);
//	print_mat(A,n);
	double * b = init_vec(n);
	
	struct timeval start, end;
	long long tot;
	gettimeofday(&start, NULL);
	gauss(A,b,n);
	gettimeofday(&end, NULL);
	tot = (end.tv_sec - start.tv_sec)*1000000L + (end.tv_usec - start.tv_usec);

	FILE * f = fopen("serial_results.csv","a");
	if (f == NULL) return -1;
	fprintf(f, "%d,%lld\n", n, tot);
	fclose(f);
	
	if (0){
		printf("row reduced matrix:\n");
		print_mat(A,n);
		printf("gaussian solution: ");
		print_vec(b,n);
	}
	printf("gaussian elimination took %lld microseconds in serial\n", tot);
	free(A[0]); //frees Adata ?
	free(A);
	return 0;
}	
