import numpy as np
import pylab as plt
import pandas as pd

"""
df1 = np.genfromtxt("serial_results.csv", delimiter=',',names=["Matrix Siz","Time"])
df2 = np.genfromtxt("parallel_results.csv", delimiter=',', names=["Matrix Size","Time"])
"""
df1 =pd.read_csv("serial_results.csv")
df2 = pd.read_csv("parallel_results.csv")

plt.plot(df1["Matrix Size"], df1["Time"], 'o', linestyle='-')
plt.plot(df2["Matrix Size"], df2["Time"], 'o', linestyle='-')
plt.xlabel("Matrix Size n")
plt.ylabel("Time (microseconds)")
plt.title("Time Taken to do Gaussian Elimination on Matrix of Size n")
plt.savefig("gauss_graph.eps")
plt.show()

#this isnt really necessary
if df1["Matrix Size"] != df2["Matrix Size"]:
	print("matrix size not the same for serial and parallel tests, please fix code")
